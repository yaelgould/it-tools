---
# tasks file for haproxy

- name: Ensure HAProxy is installed.
  package: name=haproxy state=present

- name: Ensure HAProxy is enabled (so init script will start it on Debian).
  lineinfile:
    dest: /etc/default/haproxy
    regexp: "^ENABLED.+$"
    line: "ENABLED=1"
    state: present
  when: ansible_os_family == 'Debian'

- name: Get HAProxy version.
  command: haproxy -v
  register: haproxy_version_result
  changed_when: false
  check_mode: false

- name: copy ca's crt
  copy:
    src: "/ittools/ownca/yaelCA.crt"  
    dest: "/etc/pki/ca-trust/source/anchors/yaelCA.crt" 

- name: convert ca crt to pem file
  shell: "openssl x509 -in /etc/pki/ca-trust/source/anchors/yaelCA.crt -out /etc/pki/ca-trust/source/anchors/yaelCA.pem -outform PEM"

- name: update trusted ca's
  shell: update-ca-trust

- name: generate private key
  openssl_privatekey:
    path: "/etc/pki/tls/private/{{ ansible_hostname }}.key"

- name: generate csr
  openssl_csr:
    privatekey_path: "/etc/pki/tls/private/{{ ansible_hostname }}.key"
    common_name: "{{ ansible_hostname }}"
    path: "/etc/pki/tls/private/{{ ansible_hostname }}.csr"
    subject_alt_name: "DNS:catsfood.com,DNS:gianthead.com"
    subject_alt_name_critical: yes

- name: pull csr
  fetch:
    src: "/etc/pki/tls/private/{{ ansible_hostname }}.csr"
    dest: "/ittools/ownca/{{ ansible_hostname }}.csr"
    flat: true

- name: sign csr with CA key
  delegate_to: localhost
  openssl_certificate:
    path: "/ittools/ownca/{{ ansible_hostname }}.crt"
    csr_path: "/ittools/ownca/{{ ansible_hostname }}.csr"
    ownca_path: "/ittools/ownca/yaelCA.crt"
    ownca_privatekey_path: "/ittools/ownca/yaelCA.key"
    ownca_privatekey_passphrase: "qwe123"
    provider: ownca

- name: push crt back to server
  copy:
    src: "/ittools/ownca/{{ ansible_hostname }}.crt"
    dest: "/etc/pki/tls/certs/{{ ansible_hostname }}.crt"

- name: add crt to pem file
  shell: "cat /etc/pki/tls/certs/{{ ansible_hostname }}.crt > /etc/pki/tls/certs/{{ ansible_hostname }}.pem"

- name: add key to pem file
  shell: "cat /etc/pki/tls/private/{{ ansible_hostname }}.key >> /etc/pki/tls/certs/{{ ansible_hostname }}.pem"

- name: Copy HAProxy configuration in place.
  template:
    src: haproxy.cfg.j2
    dest: /etc/haproxy/haproxy.cfg
    mode: 0644

- name: add https service
  firewalld:
    service: https
    permanent: yes
    state: enabled

- name: allow https traffic on port 443
  firewalld:
    port: 443/tcp
    permanent: yes
    state: enabled

- name: Ensure HAProxy is started and enabled on boot.
  service: name=haproxy state=restarted enabled=yes

